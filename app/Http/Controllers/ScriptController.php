<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ScriptRequest;
use App\Models\Script;
use DateInterval;
use Illuminate\Http\Response;
use judahnator\Lua\Parser;

final class ScriptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->view('scripts.index', [
            'scripts' => Script::query()
                ->scopes('popular')
                ->paginate(15)
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return response()->view('scripts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ScriptRequest $request
     */
    public function store(ScriptRequest $request)
    {
        $script = new Script($request->validated());
        $parser = Parser::setup($script->getAttribute('script'), timeLimit: new DateInterval(config('app.script_timeout')));

        $start = microtime(true);
        $result = $parser->run();
        $end = microtime(true);

        $script->setAttribute('runtime', round(($end - $start) * 1e3, 2));
        $script->setAttribute('result', $result->getValue());
        $script->setAttribute('stdout', $parser->getEnv()->STDIO->out);

        $script->saveOrFail();

        return response()->redirectToRoute('scripts.show', $script);
    }

    /**
     * Display the specified resource.
     *
     * @param Script $script
     * @return Response
     */
    public function show(Script $script)
    {
        return response()->view('scripts.show', ['script' => $script]);
    }
}
