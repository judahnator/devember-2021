<?php declare(strict_types=1);

namespace App\Http\Controllers;

use DateInterval;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use judahnator\Lua\Debug;
use judahnator\Lua\Exceptions\TimeoutException;
use judahnator\Lua\Parser;

final class PreviewController extends Controller
{
    public function lexer(Request $request)
    {
        $this->validate($request, ['script' => 'required|string']);
        return response(Debug::lexer($request->get('script')))->header('Content-Type', 'text/plain');
    }

    public function ast(Request $request)
    {
        $this->validate($request, ['script' => 'required|string']);
        return new JsonResponse(Debug::ast($request->get('script')), json: true);
    }

    public function run(Request $request)
    {
        $this->validate($request, ['script' => 'required|string']);

        $parser = Parser::setup($request->get('script'), timeLimit: new DateInterval(config('app.script_timeout')));

        try {
            $start = microtime(true);
            $result = $parser->run()->getValue();
            $end = microtime(true);
        } catch (TimeoutException $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ])->setStatusCode(408);
        }

        return response()->json([
            'runtime' => round(($end - $start) * 1e3, 2),
            'result' => (string)$result,
            'stdout' => $parser->getEnv()->STDIO->out,
        ]);
    }
}
