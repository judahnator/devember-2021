<?php declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Script;

final class KudosController
{
    /**
     * Increment kudos value.
     *
     * @param Script $script
     */
    public function store(Script $script)
    {
        $script->updateOrFail(['kudos' => $script->getAttribute('kudos') + 1]);
        return response()->json($script);
    }
}
