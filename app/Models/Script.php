<?php declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use judahnator\Lua\Debug;

final class Script extends Model
{
    use HasFactory;

    protected $casts = [
        'title' => 'string',
        'script' => 'string',
        'runtime' => 'float',
        'result' => 'string',
        'stdout' => 'string',
        'kudos' => 'int',
    ];

    protected $fillable = [
        'title',
        'script',
        'runtime',
        'result',
        'stdout',
        'kudos',
    ];

    public function getAstTokensAttribute(): string
    {
        return Debug::ast($this->getAttribute('script'));
    }

    public function getLexerTokensAttribute(): string
    {
        return Debug::lexer($this->getAttribute('script'));
    }

    public function scopePopular(Builder $query): Builder
    {
        return $query->orderByDesc('kudos');
    }
}
