<?php

use App\Http\Controllers\KudosController;
use App\Http\Controllers\PreviewController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('throttle:preview')->group(function () {
    Route::post('/preview/ast', [PreviewController::class, 'ast'])->name('preview.ast');
    Route::post('/preview/lexer', [PreviewController::class, 'lexer'])->name('preview.lexer');
    Route::post('/preview/run', [PreviewController::class, 'run'])->name('preview.run');
});

Route::post('/kudos/{script}', [KudosController::class, 'store'])
    ->name('kudos.store')
    ->middleware('throttle:kudos');
