<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Judahnators Devember 2021 Submission">
    <title>{{ config('app.name') }}</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="{{ asset('js/app.js') }}"></script>
</head>
<body>
<div class="d-flex" id="wrapper">
    <!-- Sidebar-->
    <div class="border-end bg-white" id="sidebar-wrapper">
        <div class="list-group list-group-flush">
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="{{ route('home') }}">Home</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="{{ route('scripts.index') }}">Scripts</a>
            <a class="list-group-item list-group-item-action list-group-item-light p-3" href="{{ route('scripts.create') }}">Create</a>
        </div>
    </div>

    <!-- Page content wrapper-->
    <div id="page-content-wrapper">
        <!-- Top navigation-->
        <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <div class="container-fluid">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ms-auto mt-2 mt-lg-0">
                        <li class="nav-item active"><a class="nav-link" href="{{ route('home') }}">Home</a></li>
                        <li class="nav-item">
                            <a class="nav-link" target="_blank" href="https://gitlab.com/judahnator/devember-2021">Source</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <!-- Page content-->
        <div class="container-fluid">
            <h1 class="mt-4">@yield('heading')</h1>
            @yield('content')
        </div>
    </div>
</div>

</body>
</html>
