@php /** @var \Illuminate\Pagination\LengthAwarePaginator|\App\Models\Script[] $scripts */ @endphp

@extends('layouts.main')

@section('heading', 'User Scripts')

@section('content')
    <div class="row">
        <div class="col-12">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th scope="col">Actions</th>
                    <th scope="col">Title</th>
                    <th scope="col">Runtime</th>
                    <th scope="col">Kudos</th>
                    <th scope="col">Submitted</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($scripts as $script)
                    <tr>
                        <th scope="row">
                            <a class="btn btn-primary" href="{{ route('scripts.show', $script) }}">View</a>
                        </th>
                        <td>{{ $script->getAttribute('title') }}</td>
                        <td>{{ $script->getAttribute('runtime') }}ms</td>
                        <td>{{ $script->getAttribute('kudos') }}</td>
                        <td>{{ $script->getAttribute('created_at')->format('Y-m-d') }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $scripts->links() }}
        </div>
    </div>
@endsection
