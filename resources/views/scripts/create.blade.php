@extends('layouts.main')

@section('heading', 'Script Writer')

@section('content')
    <form action="{{ route('scripts.store') }}" method="post">
        @csrf
        <div class="row">
            <div class="col-4">
                <p>Write your own script!</p>
                <label for="title">Script Title:</label>
                <input id="title" type="text" name="title" class="form-control" style="background: white;" required>
            </div>

            <div class="col-4">
                <p><b>Runtime:</b> <i id="runtime">Unknown</i></p>
                <p><b>Result:</b> <i id="result">Unknown</i></p>
                <p><b>StdOut:</b> <i id="stdout">Unknown</i></p>
            </div>

            <div class="col-4 d-grid gap-2 mx-auto">
                <button class="btn btn-secondary btn-block" type="button" onclick="preview()">Preview</button>
                <button class="btn btn-primary btn-block" type="submit">Save</button>
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <label for="script-input" class="form-label">Your Lua Script:</label>
                <textarea id="script-input" name="script"></textarea>
            </div>

            <div class="col-4">
                <label for="lexer-output" class="form-label">Lexer Output</label>
                <textarea disabled id="lexer-output"></textarea>
            </div>

            <div class="col-4">
                <label for="ast-tree" class="form-label">AST Output</label>
                <textarea disabled id="ast-tree"></textarea>
            </div>
        </div>
    </form>

    <script>
        let input = CodeMirror.fromTextArea(
            document.getElementById('script-input'),
            {
                lineNumbers: true,
                value: 'print(\r\n  "The answer to life, the universe, and everything: ",\r\n  42\r\n);',
                mode: "text/x-lua",
            }
        );

        let tokens = CodeMirror.fromTextArea(
            document.getElementById('lexer-output'),
            {
                lineNumbers: true,
                mode: null,
                readOnly: true,
            }
        );

        let astNodes = CodeMirror.fromTextArea(
            document.getElementById('ast-tree'),
            {
                lineNumbers: true,
                mode: "application/json",
                readOnly: true,
            },
        );

        async function preview () {
            try {
                const inputScript = input.getValue();

                tokens.setValue((await axios.post('{{ route('preview.lexer') }}', {script: inputScript})).data);
                astNodes.setValue(JSON.stringify((await axios.post('{{ route('preview.ast') }}', {script: inputScript})).data, null, 2));

                let result = (await axios.post('{{ route('preview.run') }}', {script: inputScript})).data
                document.getElementById('runtime').textContent = result.runtime + "ms";
                document.getElementById('result').textContent = result.result;
                document.getElementById('stdout').textContent = result.stdout;
            } catch (exception) {
                alert(exception.response.data.message);
            }
        }

        input.on("blur", preview);
    </script>
@endsection
