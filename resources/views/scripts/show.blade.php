@php /** @var \App\Models\Script $script */ @endphp

@extends('layouts.main')

@section('heading', $script->getAttribute('title'))

@section('content')
    <div class="row">
        <div class="col-4 d-grid gap-2 mx-auto p-4">
            <button class="btn btn-primary" id="kudos" type="button" onclick="kudos()">Kudos: {{ $script->getAttribute('kudos') }}</button>
        </div>

        <div class="col-6 offset-2">
            <p><b>Runtime:</b> <i id="runtime">{{ $script->getAttribute('runtime') }}ms</i></p>
            <p><b>Result:</b> <i id="result">{{ $script->getAttribute('result') }}</i></p>
            <p><b>StdOut:</b> <i id="stdout">{{ $script->getAttribute('stdout') }}</i></p>
        </div>
    </div>

    <div class="row">
        <div class="col-4">
            <label for="script-input" class="form-label">Script:</label>
            <textarea disabled id="script-input">{{ $script->getAttribute('script') }}</textarea>
        </div>

        <div class="col-4">
            <label for="lexer-output" class="form-label">Lexer Output</label>
            <textarea disabled id="lexer-output">{{ $script->getLexerTokensAttribute() }}</textarea>
        </div>

        <div class="col-4">
            <label for="ast-tree" class="form-label">AST Output</label>
            <textarea disabled id="ast-tree">{{ $script->getAstTokensAttribute() }}</textarea>
        </div>
    </div>

    <script>
        let input = CodeMirror.fromTextArea(
            document.getElementById('script-input'),
            {
                lineNumbers: true,
                mode: "text/x-lua",
                readOnly: true,
            }
        );

        let tokens = CodeMirror.fromTextArea(
            document.getElementById('lexer-output'),
            {
                lineNumbers: true,
                mode: null,
                readOnly: true,
            }
        );

        let astNodes = CodeMirror.fromTextArea(
            document.getElementById('ast-tree'),
            {
                lineNumbers: true,
                mode: "application/json",
                readOnly: true,
            },
        );

        async function kudos() {
            try {
                let response = await axios.post('{{ route('kudos.store', $script) }}')
                document.getElementById('kudos').textContent = 'Kudos: ' + response.data.kudos;
            } catch (exception) {
                alert(exception.response.data.message);
            }
        }
    </script>
@endsection
