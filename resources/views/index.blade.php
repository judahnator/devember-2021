@extends('layouts.main')

@section('heading', 'Devember - Lua Parser')

@section('content')
    @markdown
    # What can I find on this site?

    On this site you can see an option to create and share scripts you write. The window pane will periodically refresh displaying the generated lexer tokens, compiled AST tree, and interpreted results!

    # In Abstract

    This Devember project aimed at showing developers the process of interpreting, parsing, and executing computer code.
    By no means does this project conform to any set standard. The Lexer, AST Compiler, and Parser are all written by
    me, and I was not developing with interoperability in mind. This makes this mostly useful as an example, but is by
    no means meant to serve as an implementation guide.

    ## The Methodology

    In broad terms, the process goes from script to Lexer to Compiler to Parser.

    ### The Lexer

    The Lexer is a script that defines the dictionary for your langauge. It describes what operator tokens exist, how to
    work with numbers, what strings are valid, and so on. Its job is to describe the possible syntax for the language.

    ### Abstract Syntax Tree

    The AST compiler takes the result of the Lexer and composes the "words" into "ideas." In short its job is to
    identify "Expressions" and "Statements." The former represents a value, the later indicates control flow through
    the application.

    ### The Parser

    The job of the parser is to take the result of the AST actually perform the instructions defined in it.

    @endmarkdown
@endsection
